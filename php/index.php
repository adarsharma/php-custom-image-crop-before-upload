<?php
	error_reporting(-1);
	require_once 'phplibrary.php' ;
?>
<html>
	<head>
		<title>PHP upload file demo</title>
	</head>
	<body>
		<form method="post" enctype="multipart/form-data" name="formUploadFile">		
			<label>Select single file to upload:</label>
			<input type="file" name="files[]" multiple="multiple" />
			<input type="submit" value="Upload File" name="btnSubmit"/>
		</form>		
		<?php
			if(isset($_POST["btnSubmit"]))
			{
				$errors = array();
				$uploadedFiles = array();
				$extension = array("jpeg","jpg","png","gif");
				$bytes = 1024;
				$KB = 1024;
				$totalBytes = $bytes * $KB;
				$UploadFolder = "UploadFolder"; //folder to upload image
				
				$counter = 0;

				function UploadExactSize($p,$q,$img_w,$img_h,$temp,$full_path_name){

					$m = min($img_w,$img_h);
					echo '<pre> minimum of width and height'; print_r($m); echo '</pre>';

					switch($m){
						case $img_w:
							$y1 = ($img_h - $img_w ) / 2 ; 
							$x1 = 0;
							$w = $img_w;
							$h = ($img_w*$q)/$p;
							if($h > $img_h ){
								$x1 = ($img_w*$q)/($p*2);
								$y1 = 0;
								$w = $img_w;
								$h = ($img_w * $q) / $p;
							}
							$x2 = $x1+$w;
							$y2 = $y1+$h;
						break;
						case $img_h:
							$x1 = ($img_w - $img_h ) / 2 ;
							$y1 = 0;
							$h = $img_h;
							$w = ($img_h*$p)/$q;
							if($w > $img_w){
								$y1 = ($img_h*$q) / ($p*2);
								$x1 = 0 ;
								$w = $img_w;
								$h = ($img_w * $q) / $p;
							}
							$x2 = $x1+$w;
							$y2 = $y1+$h;
						break;
						default:

						break;
					} 
						echo '<pre>'; print_r($y2); echo '</pre>';
						echo '<pre>'; print_r($x2); echo '</pre>';
						echo '<pre>'; print_r($y1); echo '</pre>';
						echo '<pre>'; print_r($x1); echo '</pre>';
						$manipulator = new ImageManipulator($temp);
						$newImage = $manipulator->crop($x1, $y1, $x2, $y2);
						$manipulator->save($full_path_name);
						chmod($full_path_name,0777);  
						return true;
				};
				
				foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name){
					$temp = $_FILES["files"]["tmp_name"][$key];
					$name = $_FILES["files"]["name"][$key];
					
					if(empty($temp))
					{
						break;
					}
					
					$counter++;
					$UploadOk = true;
					
					if($_FILES["files"]["size"][$key] > $totalBytes)
					{
						$UploadOk = false;
						array_push($errors, $name." file size is larger than the 1 MB.");
					}
					
					$ext = pathinfo($name, PATHINFO_EXTENSION);
					if(in_array($ext, $extension) == false){
						$UploadOk = false;
						array_push($errors, $name." is invalid file type.");
					}
					
					if(file_exists($UploadFolder."/".$name) == true){
						$UploadOk = false;
						array_push($errors, $name." file is already exist.");
					}
					
					if($UploadOk == true){
						$image_full_path = $UploadFolder."/".$name;
						$image_info = getimagesize($temp);
						$image_width = $image_info[0];
						// echo '<pre> image size width'; print_r($image_width); echo '</pre>';
						$image_height = $image_info[1];
						// echo '<pre> image size height'; print_r($image_height); echo '</pre>';
					 	
			 UploadExactSize(16,9,$image_width,$image_height,$temp, $UploadFolder."/"."16x9-".$name); // for image ratio 16:9  
			 UploadExactSize(10,2,$image_width,$image_height,$temp, $UploadFolder."/"."10x2-".$name); // for image ratio 16:9  
			 UploadExactSize(1,1,$image_width,$image_height,$temp, $UploadFolder."/"."1x1-".$name); // for image ratio 1:1 
						 
					}
				}
				
				if($counter>0){
					if(count($errors)>0)
					{
						echo "<b>Errors:</b>";
						echo "<br/><ul>";
						foreach($errors as $error)
						{
							echo "<li>".$error."</li>";
						}
						echo "</ul><br/>";
					}
					
					if(count($uploadedFiles)>0){
						echo "<b>Uploaded Files:</b>";
						echo "<br/><ul>";
						foreach($uploadedFiles as $fileName)
						{
							echo "<li>".$fileName."</li>";
						}
						echo "</ul><br/>";
						
						echo count($uploadedFiles)." file(s) are successfully uploaded.";
					}								
				}
				else{
					echo "Please, Select file(s) to upload.";
				}
			}
		?>
	</body>
</html>